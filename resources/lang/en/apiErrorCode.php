<?php
$APICode = array(

    //create user begin
    'users_code_required_email' => 'Email is required',
    'users_code_unique_email' => 'Duplicate email',
    'users_code_email_email' => 'Invalid email',

    'users_code_required_name' => 'Name\'s required',
    'users_code_min_name' => 'Name length must be greater than 3 chars',
    'users_code_max_name' => 'Name lenght must be less than 50',
    //create user end


);
$apiErrorCodes = array(
    //create user begin
    'users_code_required_email' =>1000,
    'users_code_unique_email' => 1001,
    'users_code_email_email' => 1002,

    'users_code_required_name' => 1003,
    'users_code_min_name' => 1004,
    'users_code_max_name' => 1005,
    //create user end


);
$APICode['ApiErrorCodes'] = $apiErrorCodes;
$apiErrorCodesFlip = array_flip($apiErrorCodes);
$APICode['ApiErrorCodesFlip'] = $apiErrorCodesFlip;

return $APICode;