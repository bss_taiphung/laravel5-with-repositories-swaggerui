<?php

namespace App\Repositories\Eloquent;

use App\Common\ValidationCustom;
use Illuminate\Support\Facades\Validator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserRepository;
use App\Models\User;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Specify Validator Rules
     * @var array
     */
    protected static  $rulesCustom = [
        'RULE_CREATE' => [
            'email' => 'required|email|unique:users,email',
            'name' => 'required|min:3|max:50'
        ],
        'RULE_UPDATE' => [
            'name' => 'required|min:3|max:50',
            'email' => 'required|unique:users,email,:id,id'
        ]
    ];


    /**
     * Get Validator Rules
     * @var string $actionRule
     * @var array $data
     * @var array $messages
     * @var array $mergeRules
     * @var array $bindParams
     *
     * @return array
     */
    public static function validateCustom($actionRule = '',$data = [],$messages =[],  $mergeRules = [], $bindParams = [])
    {
        $role = ValidationCustom::getRules(static::$rulesCustom, $actionRule, $mergeRules, $bindParams);
        $validator = Validator::make($data, $role, $messages);
        return ValidationCustom::getErrors($validator);
    }

}
