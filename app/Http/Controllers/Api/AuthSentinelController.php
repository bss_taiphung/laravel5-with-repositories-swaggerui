<?php namespace App\Http\Controllers\Api;


use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Swagger\Annotations as SWG;
use App\Http\Controllers\ApiBaseController;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   resourcePath="/Auth",
 *   description="Auth api",
 *   produces="['application/json']"
 * )
 *
 */
class AuthSentinelController extends ApiBaseController
{


    public function __construct(Sentinel $sentinel, Activation $activation)
    {
        $this->sentinel = $sentinel;
        $this->activation = $activation;
    }



    /**
     * @SWG\Model(
     *    id="register_auth_model",
     * 	@SWG\Property(name="email", type="string", required=true, defaultValue="taipv.uit@gmail.com"),
     * 	@SWG\Property(name="password", type="string", required=true, defaultValue="123456")
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/auth/register",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Register User",
     *      nickname="Register User",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="register_auth_model", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function register()
    {
        try {
            $data = Input::json()->all();
            $credentials = [
                'email' => $data['email'],
                'password' => $data['password'],
            ];

            $results = $this->sentinel->register($credentials);
            $activation =  $this->activation->create((array)$results);
            return $this->respondWithSuccess($activation);
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }
    }


    /**
     * @SWG\Model(
     *    id="login_auth_model",
     * 	@SWG\Property(name="email", type="string", required=true, defaultValue="taipv.uit@gmail.com"),
     * 	@SWG\Property(name="password", type="string", required=true, defaultValue="123456")
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/auth/login",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Login Email",
     *      nickname="Login Email",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="login_auth_model", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function login()
    {
        try {
            $data = Input::json()->all();
            $credentials = [
                'email' => $data['email'],
                'password' => $data['password'],
            ];

            $results = $this->sentinel->authenticate($credentials);
            return $this->respondWithSuccess($results);
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }
    }



}