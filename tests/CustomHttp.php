<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CustomHttp extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {

        /**
         * @param Custom HTTP Requests
         *
         * */
        $response = $this->call('GET', 'api/user', array('userId' => 1));
        $this->assertEquals(200, $response->status());
    }
}
