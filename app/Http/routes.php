<?php


Route::group([
    'namespace' => 'Api',
    'prefix' => 'api',
    'middleware' => ['cors']
], function () {
    Route::group([
        'prefix' => 'auth'
    ], function () {

        Route::post('register','AuthSentinelController@register');
        Route::post('login','AuthSentinelController@login');

    });


    Route::group([
        'prefix' => 'user'
    ], function () {

        Route::get('', 'UsersController@getList');
        Route::get('{id}', 'UsersController@getDetail');

        Route::post('','UsersController@create');
        Route::put('{id}','UsersController@update');
        Route::delete('{id}','UsersController@delete');

    });

});

