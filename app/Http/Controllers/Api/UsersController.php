<?php namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Swagger\Annotations as SWG;
use App\Http\Controllers\ApiBaseController;
use App\Repositories\Eloquent\UserRepositoryEloquent;

/**
 * @SWG\Resource(
 *   apiVersion="1.0.0",
 *   swaggerVersion="1.2",
 *   resourcePath="/User",
 *   description="User API",
 *   produces="['application/json']"
 * )
 *
 */
class UsersController extends ApiBaseController
{
    protected $userRepositoryEloquent;

    public function __construct(UserRepositoryEloquent $userRepositoryEloquent)
    {
        $this->userRepositoryEloquent = $userRepositoryEloquent;
    }

    /**
     * @SWG\Api(
     *   path="/api/user",
     *   @SWG\Operation(
     *     summary="Get list user",
     *     method="GET",
     *     nickname="getListUser",
     *     @SWG\Parameter(name="skip", description="Skip value", required=false, type="string", paramType="query", allowMultiple=false),
     *     @SWG\Parameter(name="take", description="Take value", required=false, type="string", paramType="query", allowMultiple=false),
     *     @SWG\Parameter(name="query", description="Query value ", type="string",required=false, paramType="query", allowMultiple=false, defaultValue=null),
     *     @SWG\Parameter(name="orderBy", description="Order by value format is '+firstname' or '-email'", type="string",required=false,paramType="query", allowMultiple=false, defaultValue=null),
     *     @SWG\ResponseMessage(code=200, message="OK"),
     *     @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *     @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *     @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */

    public function getList()
    {
        try {
            $results = $this->userRepositoryEloquent->all();
            return $this->respondWithSuccess($results);
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }
    }

    /**
     * @SWG\Api(
     *   path="/api/user/{id}",
     *   @SWG\Operation(
     *     summary="Get  user detail",
     *     method="GET",
     *     nickname="getDetail",
     *     @SWG\Parameter(name="id", description="User ID", required=true, type="integer", paramType="path", allowMultiple=false),
     *     @SWG\ResponseMessage(code=200, message="OK"),
     *     @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *     @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *     @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */

    public function getDetail($userId)
    {
        try {
            $results = $this->userRepositoryEloquent->find($userId);
            return $this->respondWithSuccess($results);
        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }
    }




    /**
     * @SWG\Model(
     *    id="create",
     * 	@SWG\Property(name="name", type="string", required=true, defaultValue="Johnny Depp"),
     * 	@SWG\Property(name="email", type="string", required=true, defaultValue="test@mail.com")
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/user",
     *   @SWG\Operation(
     *      method="POST",
     *      summary="Create User",
     *      nickname="json-create",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="create", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function create()
    {
        try {

            $errorCode = Lang::get('apiErrorCode');

            $actionRule = 'RULE_CREATE';
            $data = Input::json()->all();
            $messages = array(
                'email.email' => $errorCode['ApiErrorCodes']['users_code_email_email'],
                'email.required' => $errorCode['ApiErrorCodes']['users_code_required_email'],
                'email.unique' => $errorCode['ApiErrorCodes']['users_code_unique_email'],
                'name.required' => $errorCode['ApiErrorCodes']['users_code_required_name'],
                'name.min' => $errorCode['ApiErrorCodes']['users_code_min_name'],
                'name.max' => $errorCode['ApiErrorCodes']['users_code_max_name']
            );
            $mergeRules = [];
            $bindParams = [];

            $validatorErrors = $this->userRepositoryEloquent->validateCustom($actionRule, $data, $messages, $mergeRules, $bindParams);

            if (empty($validatorErrors)) {
                //ok
                $results = $this->userRepositoryEloquent->create($data);
            } else {
                return $this->respondWithError($validatorErrors, 200);
            }
            return $this->respondWithSuccess($results);

        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }
    }

    /**
     * @SWG\Model(
     *    id="update",
     *  @SWG\Property(name="id", type="string", required=true, defaultValue="98"),
     * 	@SWG\Property(name="name", type="string", required=true, defaultValue="Johnny Depp"),
     * 	@SWG\Property(name="email", type="string", required=true, defaultValue="test@mail.com"),
     * )
     */

    /**
     * @SWG\Api(
     *   path="/api/user/{id}",
     *   @SWG\Operation(
     *      method="PUT",
     *      summary="Update User",
     *      nickname="json-update",
     *      @SWG\Parameter(name="body", description="Request body", required=true, type="update", paramType="body", allowMultiple=false),
     *      @SWG\ResponseMessage(code=200, message="OK"),
     *      @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *      @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *      @SWG\ResponseMessage(code=404, message="Resource not found")
     *   )
     * )
     */
    public function update()
    {
        try {
            $errorCode = Lang::get('apiErrorCode');

            $actionRule = 'RULE_UPDATE';
            $data = Input::json()->all();
            $messages = array(
                'email.email' => $errorCode['ApiErrorCodes']['users_code_email_email'],
                'email.required' => $errorCode['ApiErrorCodes']['users_code_required_email'],
                'email.unique' => $errorCode['ApiErrorCodes']['users_code_unique_email'],
                'name.required' => $errorCode['ApiErrorCodes']['users_code_required_name'],
                'name.min' => $errorCode['ApiErrorCodes']['users_code_min_name'],
                'name.max' => $errorCode['ApiErrorCodes']['users_code_max_name']
            );
            $mergeRules = [];
            $bindParams = [
                ":id" =>$data['id']
            ];

            $validatorErrors = $this->userRepositoryEloquent->validateCustom($actionRule, $data, $messages, $mergeRules, $bindParams);

            if (empty($validatorErrors)) {
                //ok
                $results = $this->userRepositoryEloquent->update($data,$data['id']);
            } else {
                return $this->respondWithError($validatorErrors, 200);
            }
            return $this->respondWithSuccess($results);

        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }

    }


    /**
     * @SWG\Api(
     *     path="/api/user/{id}",
     *     @SWG\Operation(
     *      summary="Delete user.",
     *      method="DELETE",
     *      nickname="deleteUser",
     *     @SWG\Parameter(name="id", description="User Id", required=true, type="integer",paramType="path",allowMultiple=false),
     *     @SWG\ResponseMessage(code=400, message="Invalid request params"),
     *     @SWG\ResponseMessage(code=401, message="Caller is not authenticated"),
     *     @SWG\ResponseMessage(code=404, message="Resource not found")
     * ))
     */
    public function delete($id)
    {
        try {
            $results =$this->userRepositoryEloquent->findWhere(array('id'=>$id))->first();
            if(empty($results)){
                return $this->errorNotFound();
            }else{
                $results->delete();
                return $this->respondWithSuccess($results);
            }

        } catch (\Exception $e) {
            return $this->respondWithErrorMessage($e->getMessage(), 400);
        }

    }
}